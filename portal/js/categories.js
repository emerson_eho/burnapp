﻿app.controller('categories', function ($scope, $http, firebaseDB, $q) {

    firebase.database().ref("eventos").once('value', function (snapshot) {
        $scope.questoes = snapshot.val(); 
        $scope.$evalAsync();
    });

    firebase.database().ref("categorias").once('value', function (snapshot) {
        $scope.categorias = snapshot.val(); 
        console.log($scope.categorias);
        $scope.$evalAsync();
    });

    $scope.addNewCategory = function () {

        var category = {};

        category.nome = $('#categoryTitle').val();
        category.tags = '[' + $('#categoryTags').val() + ']';
        category.cor = $('#categoryColor').val();

       var promise = firebaseDB.create('categorias', category);
            $q.all([promise]).then(data => {
            console.log("Adicionado com sucesso!");
        });
    }


    function edit(registro) {
        var promise = firebaseDB.update(tabela, registro);
        $q.all([promise]).then(data => {
            console.log("Editado com sucesso!");
        })
    }


    
});