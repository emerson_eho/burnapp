﻿app.controller('home', function ($scope,$q) {
    firebase.database().ref("eventos").on('value', function (snapshot) {
        $scope.questoes =  Object.entries(snapshot.val()).map(e => Object.assign(e[1], {key: e[0]}));
        $scope.$evalAsync();
    });



    function buildGraphicByUser(paramsData, paramsGraphicType, paramsIdDestination){
        if(paramsGraphicType=="column"){
        // Create the  Bar chart
        Highcharts.chart(paramsIdDestination, {
            chart: {
                type: 'column'
            },
            title: {
                text: paramsData.question
            },
            subtitle: {
                text: paramsData.event
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Quantitativo'
                }
    
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
    
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
    
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: "Like",
                    y: paramsData.valuesYesNo.like
                }, {
                    name: "UnLike",
                    y: paramsData.valuesYesNo.unlike
                },{
                    name: "Love",
                    y: paramsData.valuesYesNo.love
                }]
            }]
        });
        }
        if(paramsGraphicType=="spiderweb"){
            Highcharts.chart(paramsIdDestination, {
    
                chart: {
                    polar: true,
                    type: 'line'
                },
    
                title: {
                    text: paramsData.question,
                    x: -80
                },
    
                pane: {
                    size: '80%'
                },
    
                xAxis: {
                    categories: ['Raiva', 'Medo', 'Alegria', 'Analítica',
                        'Confiante', 'Tristeza','Tentativa'],
                    labels: {
                        style: {
                            fontSize: '18px',
                            fontFamily: 'Verdana, sans-serif',
                            width: 150
                        }
                    },
                    tickmarkPlacement: 'on',
                    lineWidth: 0
                },
    
                yAxis: {
                    gridLineInterpolation: 'polygon',
                    lineWidth: 0,
                    min: 0
                },
    
                tooltip: {
                    shared: true
                    //pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
                },
    
                legend: {
                    align: 'right',
                    verticalAlign: 'top',
                    y: 100,
                    layout: 'vertical'
                },
    
                series: [{
                    name: 'Fellings',
                    data: [paramsData.valuesFeelings.anger,paramsData.valuesFeelings.fear,paramsData.valuesFeelings.joy,
                    paramsData.valuesFeelings.analytical,paramsData.valuesFeelings.confident,paramsData.valuesFeelings.sadness,paramsData.valuesFeelings.tentative],
                    pointPlacement: 'on'
                }]
    
            }); 
        }
    };

    $scope.addSiperGeralChart = function(data,id){

        var quant = 0;
        var pseudoData = {
            event:  '',
            valuesYesNo: {
                like:0,
                unlike:0,
                love:0},
            valuesFeelings: {
                anger: 0,
                fear: 0,
                joy: 0,
                analytical: 0,
                confident: 0,
                sadness: 0,
                tentative:0
            }
        };

        data.forEach(function(x,y){
            pseudoData.event = x.nome
            x = Object.entries(x.perguntas).map(e => Object.assign(e[1], {key: e[0]}));
            x.forEach(function(x,y){
                if(x.respostas){
                    x = Object.entries(x.respostas).map(e => Object.assign(e[1], {key: e[0]}));
                    x.forEach(function(x,y){

                        if(x.react == "like"){pseudoData.valuesYesNo.like = pseudoData.valuesYesNo.like+1;}
                        if(x.react == "unlike"){pseudoData.valuesYesNo.unlike = pseudoData.valuesYesNo.unlike+1;}
                        if(x.react == "love"){pseudoData.valuesYesNo.love = pseudoData.valuesYesNo.love+1;}

                        x.document_tone.tones.forEach(function(x,y){
                            if(x.tone_id == "anger"){pseudoData.valuesFeelings.anger = pseudoData.valuesFeelings.anger+x.score;}
                            if(x.tone_id == "fear"){pseudoData.valuesFeelings.fear = pseudoData.valuesFeelings.fear+x.score}
                            if(x.tone_id == "joy"){pseudoData.valuesFeelings.joy = pseudoData.valuesFeelings.joy+x.score}
                            if(x.tone_id == "analytical"){pseudoData.valuesFeelings.analytical = pseudoData.valuesFeelings.analytical+x.score}
                            if(x.tone_id == "confident"){pseudoData.valuesFeelings.confident = pseudoData.valuesFeelings.confident+x.score}
                            if(x.tone_id == "sadness"){pseudoData.valuesFeelings.sadness = pseudoData.valuesFeelings.sadness+x.score}
                            if(x.tone_id == "tentative"){pseudoData.valuesFeelings.tentative = pseudoData.valuesFeelings.tentative+x.score}
                        });
                        quant = quant+1;
                    });
                }
            });
                
            pseudoData.valuesFeelings.anger = pseudoData.valuesFeelings.anger/quant;
            pseudoData.valuesFeelings.fear = pseudoData.valuesFeelings.fear/quant;
            pseudoData.valuesFeelings.joy = pseudoData.valuesFeelings.joy/quant;
            pseudoData.valuesFeelings.analytical = pseudoData.valuesFeelings.analytical/quant;
            pseudoData.valuesFeelings.confident = pseudoData.valuesFeelings.confident/quant;
            pseudoData.valuesFeelings.sadness = pseudoData.valuesFeelings.sadness/quant;
            pseudoData.valuesFeelings.tentative = pseudoData.valuesFeelings.tentative/quant;

               // Falta adicionar na div, passarid no ultimo parametro
               buildGraphicByUser(pseudoData,'spiderweb',id);

               $(

                 `<table class="table table-responsive">
                    <thead>
                        <tr>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> 
                                <label  style="font-size: 18pt;">`+pseudoData.valuesYesNo.unlike+`</label>
                                <i class="far fa-thumbs-down" style="font-size: 2.73em;"></i> 
                            </td>
                            <td>
                                <label  style="font-size: 18pt;">`+pseudoData.valuesYesNo.like+`</label>
                                <i class="far fa-thumbs-up" style="font-size: 2.73em;color:#408DA9 "></i>
                            </td>
                            <td>
                                <label  style="font-size: 18pt;">`+pseudoData.valuesYesNo.love+`</label>
                                <i class="far fa-heart" style="font-size: 2.73em;color:#E16681"></i>
                            </td>
                            <td>
                                <label style="font-size: 18pt;">Total de respostas: `+quant+`</label>
                            </td>
                        </tr>
                    </tbody>
                </table>`

               ).appendTo($('#' + id));

        }); 
        
        
        
    }

    function addSingleCharts(data){
        console.log(data);
        var quant = 0;
        var pseudoData = {
            event:  '',
            valuesYesNo: {
                like:0,
                unlike:0,
                love:0},
            valuesFeelings: {
                anger: 0,
                fear: 0,
                joy: 0,
                analytical: 0,
                confident: 0,
                sadness: 0,
                tentative:0
            }
        };
        
        data = Object.entries(data).map(e => Object.assign(e[1], {key: e[0]}));
        
        data.forEach(function(x,y){
            pseudoData.event = y.nome
            y.perguntas.forEach(function(x,y){
                y.respostas.forEach(function(x,y){
                    if(y.tone_id == "like"){pseudoData.valuesYesNo.like = pseudoData.valuesYesNo.like+1;}
                    if(y.tone_id == "unlike"){pseudoData.valuesYesNo.unlike = pseudoData.valuesYesNo.unlike+1;}
                    if(y.tone_id == "love"){pseudoData.valuesYesNo.love = pseudoData.valuesYesNo.love+1;}
                    y.document_tone.tones.forEach(function(x,y){
                        if(y.tone_id == "anger"){pseudoData.valuesFeelings.anger = pseudoData.valuesFeelings.anger+y.score;}
                        if(y.tone_id == "fear"){pseudoData.valuesFeelings.fear = pseudoData.valuesFeelings.fear+y.score}
                        if(y.tone_id == "joy"){pseudoData.valuesFeelings.joy = pseudoData.valuesFeelings.joy+y.score}
                        if(y.tone_id == "analytical"){pseudoData.valuesFeelings.analytical = pseudoData.valuesFeelings.analytical+y.score}
                        if(y.tone_id == "confident"){pseudoData.valuesFeelings.confident = pseudoData.valuesFeelings.confident+y.score}
                        if(y.tone_id == "sadness"){pseudoData.valuesFeelings.sadness = pseudoData.valuesFeelings.sadness+y.score}
                        if(y.tone_id == "tentative"){pseudoData.valuesFeelings.tentative = pseudoData.valuesFeelings.tentative+y.score}
                    });
                    quant = quant+1;
                });
                pseudoData.anger = pseudoData.anger/quant;
                pseudoData.fear = pseudoData.fear/quant;
                pseudoData.joy = pseudoData.joy/quant;
                pseudoData.analytical = pseudoData.analytical/quant;
                pseudoData.confident = pseudoData.confident/quant;
                pseudoData.sadness = pseudoData.sadness/quant;
                pseudoData.tentative = pseudoData.tentative/quant;
                quant = 0;
                   // Falta adicionar na div, passarid no ultimo parametro
                    buildGraphicByUser('pseudoData','column','body');
            });    

        }); 
        
        
        
    }

});