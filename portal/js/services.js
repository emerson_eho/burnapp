app.service('firebaseDB', function ($q) {
    var deferred = $q.defer();
    this.create = function (table, data) {
            return firebase.database().ref(table).push(data);
        },
        this.read = function (table) {

            var query = firebase.database().ref(table);

            query.on('value', function (snapshot) {
                var data = snapshot.val();
                if (data) {
                    deferred.resolve(data);
                } else {
                    deferred.reject();
                }
            });
            return deferred.promise;

        },

        this.update = function (table, data) {
            var id = data.key;
            delete data.key;
            delete data.$$hashKey;
            return firebase.database().ref(table).child(id).update(data);
        },
        this.delete = function (table, id) {
            return firebase.database().ref(table).child(id).remove();
        }
})
